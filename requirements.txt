beautifulsoup4 >= 4.6.0; python_version >= '3.6'
requests >= 2.18.4
schedule >=  0.5.0
lxml >= 4.2.1