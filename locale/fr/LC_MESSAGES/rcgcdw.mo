��    %      D  5   l      @     A     \  "   s     �     �     �     �  	   �  
   �  "   �  "   	  (   ,     U  	   b     l     x  
   �  
   �     �  
   �     �     �     �     �  	   �     �  
   �                $     1     :     I  
   O     Z  
   h  �  s     �  &     "   =     `     y     �     �     �     �  ,   �  ,     4   5     j     |     �     �  
   �  
   �     �     �     �  
   �     �  	   	  	   	     (	     /	     <	     U	  	   d	     n	     w	     �	     �	     �	     �	                       %                                            #                             "                 	          !                   $               
                       ({} action)  ({} actions)  ({} edit)  ({} edits)  UTC ({} action)  UTC ({} actions) Admin actions But nobody came Bytes changed Daily overview Day score Edits made Most active hour Most active hours Most active user Most active users Most edited article Most edited articles New articles New files No activity Unique contributors autopatrol autoreview bot bureaucrat century centuries day days decade decades director directors editor hour hours millennium millennia minute minutes month months reviewer second seconds sysop week weeks wiki_guardian year years Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-09-03 13:14+0200
Last-Translator: Frisk <>
Language-Team: 
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Generated-By: pygettext.py 1.5
X-Generator: Poedit 2.4.1
X-Poedit-Basepath: ../../..
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-SearchPath-0: rcgcdw.pot
   ({} action)  ({} actions)  ({} modification)  ({} modifications)  UTC ({} action)  UTC ({} actions) Actions d'administrateur Aucune activité Octets modifiés Résumé de la journée Score du jour Modifications effectuées Heure la plus active Heures les plus actives Membre le plus actif Membres les plus actifs Article le plus modifié Articles les plus modifiés Nouveaux articles Nouveaux fichiers Aucune activité Contributeurs uniques autopatrol autoreview Robot Bureaucrate centenaire centenaires jour jours décennie décennies Directeur Directeur editor heure heures millénaire millénaires minute minutes mois mois reviewer seconde secondes Administrateur semaine semaines Gardien du wiki année années 