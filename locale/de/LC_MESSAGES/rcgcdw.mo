��    &      L  5   |      P     Q     l  "   �     �     �     �     �  	   �  
   �  "   �  "     (   <     e  	   r     |     �  
   �  
   �     �  
   �     �     �     �     �  	   �     �  
             %     4     A     J     Y  
   _     j  
   x     �  �  �     [  #   y  %   �     �     �     �     �  	                 $   A  4   f     �     �     �     �  
   �     �     �  	   �      	     	     "	     7	     @	     I	     P	     _	     x	     �	     �	     �	     �	     �	     �	  
   �	     �	                       %                                            #                   &         "                 	          !                   $               
                       ({} action)  ({} actions)  ({} edit)  ({} edits)  UTC ({} action)  UTC ({} actions) Admin actions But nobody came Bytes changed Daily overview Day score Edits made Most active hour Most active hours Most active user Most active users Most edited article Most edited articles New articles New files No activity Unique contributors autopatrol autoreview bot bureaucrat century centuries day days decade decades director directors editor hour hours millennium millennia minute minutes month months reviewer second seconds sysop week weeks wiki_guardian year years {value} (avg. {avg}) Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-09-03 13:14+0200
Last-Translator: MarkusRost <>
Language-Team: German <https://weblate.frisk.space/projects/rcgcdw/main/de/>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 2.4.1
X-Loco-Source-Locale: de_DE
Generated-By: pygettext.py 1.5
X-Loco-Parser: loco_parse_po
  (eine Aktion)  ({} Aktionen)  (eine Änderung)  ({} Änderungen)  UTC (eine Aktion)  UTC ({} Aktionen) Admin-Aktionen Keine Aktivität Bytes geändert Tägliche Übersicht Tageswert Bearbeitungen Aktivste Stunde Aktivste Stunden Aktivster Benutzer Aktivste Benutzer Meist bearbeiteter Artikel Meist bearbeitete Artikel Neue Artikel Neue Dateien Keine Aktivität Einzelne Autoren autopatrol Passive Sichter Bot Bürokrat Jahrhundert Jahrhunderte Tag Tage Jahrzehnt Jahrzehnte Direktor Direktor editor Stunde Stunden Jahrtausend Jahrtausende Minute Minuten Monat Monate Prüfer Sekunde Sekunden Administrator Woche Wochen Wiki Guardian Jahr Jahre {value} (vgl. {avg}) 