��            )   �      �     �     �  "   �     �     �          
           %     *     /     K     X     h     z     �     �     �  b   �  ?     W   M  a   �  M     �   U  @   �  Q   $     v     �  �  �     R     o  +   �     �     �     �     �     	     	     	  %    	     F	     U	     l	     	     �	     �	     �	  q   �	  J   &
  ^   q
  q   �
  h   B  �   �  M   L  d   �     �                
                                                     	                                                                          **Blocked user** **IP range blocked** **Removed from privileged groups** Account creation Auto account creation Deletion Disallowed the action Edit Move None Removed autoconfirmed group Stash upload Tagged the edit Throttled actions Unknown Unregistered user Upload Warning issued [{author}]({author_url}) created [{article}]({edit_link}){comment} ({bold}{sign}{edit_size}{bold}) [{author}]({author_url}) deleted [{page}]({page_link}){comment} [{author}]({author_url}) deleted redirect by overwriting [{page}]({page_link}){comment} [{author}]({author_url}) edited [{article}]({edit_link}){comment} ({bold}{sign}{edit_size}{bold}) [{author}]({author_url}) reverted a version of [{file}]({file_link}){comment} [{author}]({author_url}) triggered *{abuse_filter}*, performing the action "{action}" on *[{target}]({target_url})* - action taken: {result}. [{author}]({author_url}) uploaded [{file}]({file_link}){comment} [{author}]({author_url}) uploaded a new version of [{file}]({file_link}){comment} with a redirect without making a redirect Project-Id-Version: Turkish (RcGcDw)
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2020-11-28 22:44+0000
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: Turkish <https://weblate.frisk.space/projects/rcgcdw/main/tr/>
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 4.2.1
 **Engellenmiş kullanıcı** **IP aralığı engellendi** **Ayrıcalıklı gruplardan kaldırıldı** Hesap oluşturulması Otomatik hesap oluşturulması Siliniş Eyleme izin verilmedi Düzenle Taşı Yok Otomatik onaylanan grup kaldırıldı Çoklu yükeme Düzenleme etiketlendi Kısılı eylemler Meçhul Kayıtsız kullanıcı Yükle Uyarı verildi [{author}]({author_url}), [{article}]({edit_link}) makalesini oluşturdu{comment} ({bold}{sign}{edit_size}{bold}) [{author}]({author_url}), [{page}]({page_link}) sayfasını sildi{comment} [{author}]({author_url}), [{page}]({page_link}) üstüne yazarak yönlendirmeyi sildi{comment} [{author}]({author_url}), [{article}]({edit_link}) makalesini düzenledi{comment} ({bold}{sign}{edit_size}{bold}) [{author}]({author_url}), [{file}]({file_link}) dosyasının eski bir sürümünü geri getirdi{comment} [{author}]({author_url}), *[{target}]({target_url})* adresinde "{action}" eylemini yaparak *{abuse_filter}* tetiklemesine sebep oldu - alınan önlem: {result}. [{author}]({author_url}), [{file}]({file_link}) dosyasını yükledi{comment} [{author}]({author_url}), [{file}]({file_link}) dosyasının yeni bir sürümünü yükledi{comment} yönlendirme ile yönlendirme bırakmadan 