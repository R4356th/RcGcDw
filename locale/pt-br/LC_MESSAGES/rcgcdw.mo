��    &      L  5   |      P     Q     l  "   �     �     �     �     �  	   �  
   �  "   �  "     (   <     e  	   r     |     �  
   �  
   �     �  
   �     �     �     �     �  	   �     �  
             %     4     A     J     Y  
   _     j  
   x     �  �  �     h     �  "   �     �     �     �                3  !   D  )   f  )   �     �     �     �     �     �     	     +	  	   1	     ;	     L	     U	     f	  	   n	     x	  
   	     �	     �	  
   �	     �	     �	     �	     �	     �	     �	     
                                                             !                  #      &                 "           	                               $             
             %         ({} action)  ({} actions)  ({} edit)  ({} edits)  UTC ({} action)  UTC ({} actions) Admin actions But nobody came Bytes changed Daily overview Day score Edits made Most active hour Most active hours Most active user Most active users Most edited article Most edited articles New articles New files No activity Unique contributors autopatrol autoreview bot bureaucrat century centuries day days decade decades director directors editor hour hours millennium millennia minute minutes month months reviewer second seconds sysop week weeks wiki_guardian year years {value} (avg. {avg}) Project-Id-Version: Portuguese (Brazil) (RcGcDw)
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2020-11-27 13:07+0000
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: Portuguese (Brazil) <https://translate.wikibot.de/projects/rcgcdw/main/pt-br/>
Language: pt-br
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Generator: Weblate 4.2.1
  ({} ação)  ({} ações)  ({} edição)  ({} edições)  UTC ({} ação)  UTC ({} ações) Ações de administração Ninguém apareceu por aqui Bytes alterados Visão geral diária Pontuação do dia Edições feitas Hora mais ativa Horas mais ativas Usuário mais ativo Usuários mais ativos Artigo mais editado Artigos mais editados Novos artigos Novos arquivos Sem atividade Contribuidores exclusivos patrulheiro automático revisor automático robô burocrata século séculos dia dias década décadas diretor diretores editor hora horas milênio milênios minuto minutos mês meses revisor segundo segundos administrador semana semanas guardião da wiki ano anos {value} (med. {avg}) 