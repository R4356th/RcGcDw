��          �   %   �      P     Q     h  *   z     �     �  	   �     �  -   �  !   $  !   F     h     m  d   �     �  P   �  �   M  c   �  �   W     �  �   y  W   p  W   �           >     F  �  N     4
     J
  /   Z
     �
     �
  
   �
     �
  3   �
  '     $   :  	   _     i  i   �     �  N   
  �   Y  d     �   h  �     �   �  [   �  V   �     F     d     q        	      
                                                                                                               Commented on {article} Created "{title}" Created "{title}" on {user}'s Message Wall Created a poll "{title}" Created a quiz "{title}" Option {} Replied to "{title}" Replied to "{title}" on {user}'s Message Wall Replied to a comment on {article} Report this on the support server Tags Unknown event `{event}` Unknown event `{event}` by [{author}]({author_url}), report it on the [support server](<{support}>). Unregistered user [{author}]({author_url}) created [{title}](<{url}f/p/{threadId}>) in {forumName} [{author}]({author_url}) created [{title}](<{url}wiki/Message_Wall:{user_wall}?threadId={threadId}>) on [{user}'s Message Wall](<{url}wiki/Message_Wall:{user_wall}>) [{author}]({author_url}) created a [comment](<{url}?commentId={commentId}>) on [{article}](<{url}>) [{author}]({author_url}) created a [reply](<{url}?commentId={commentId}&replyId={replyId}>) to a [comment](<{url}?commentId={commentId}>) on [{article}](<{url}>) [{author}]({author_url}) created a [reply](<{url}f/p/{threadId}/r/{postId}>) to [{title}](<{url}f/p/{threadId}>) in {forumName} [{author}]({author_url}) created a [reply](<{url}wiki/Message_Wall:{user_wall}?threadId={threadId}#{replyId}>) to [{title}](<{url}wiki/Message_Wall:{user_wall}?threadId={threadId}>) on [{user}'s Message Wall](<{url}wiki/Message_Wall:{user_wall}>) [{author}]({author_url}) created a poll [{title}](<{url}f/p/{threadId}>) in {forumName} [{author}]({author_url}) created a quiz [{title}](<{url}f/p/{threadId}>) in {forumName} __[View image]({image_url})__ unknown {} tags Project-Id-Version: Portuguese (Brazil) (RcGcDw)
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2020-11-27 13:07+0000
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: Portuguese (Brazil) <https://translate.wikibot.de/projects/rcgcdw/discussion_formatters-1/pt-br/>
Language: pt-br
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Weblate 4.2.1
 Comentou em {article} Criou "{title}" Criou "{title}" no mural de mensagens de {user} Criou uma pesquisa "{title}" Criou um quiz "{title}" Opção {} Respondeu "{title}" Respondeu "{title}" no mural de mensagens de {user} Respondeu a um comentário em {article} Reportar isso no servidor de suporte Etiquetas Evento `{event}` desconhecido Evento `{event}` desconhecido por [{author}]({author_url}), relate no [servidor de suporte](<{support}>). Usuário não registrado [{author}]({author_url}) criou [{title}](<{url}f/p/{threadId}>) em {forumName} [{author}]({author_url}) criou [{title}](<{url}wiki/Message_Wall:{user_wall}?threadId={threadId}>) no mural de mensagens de [{user}(<{url}wiki/Message_Wall:{user_wall}>) [{author}]({author_url}) fez um [comentário](<{url}?commentId={commentId}>) em [{article}](<{url}>) [{author}]({author_url}) criou uma [resposta](<{url}?commentId={commentId}&replyId={replyId}>) a um [comentário](<{url}?commentId={commentId}>) em [{article}](<{url}>) [{author}]({author_url}) criou uma [resposta](<{url}f/p/{threadId}/r/{postId}>) a [{title}](<{url}f/p/{threadId}>) em {forumName} [{author}]({author_url}) criou uma [resposta](<{url}wiki/Message_Wall:{user_wall}?threadId={threadId}#{replyId}>) a [{title}](<{url}wiki/Message_Wall:{user_wall}?threadId={threadId}>) no mural de mensagens de [{user}](<{url}wiki/Message_Wall:{user_wall}>) [{author}]({author_url}) criou uma pesquisa [{title}](<{url}f/p/{threadId}>) em {forumName} [{author}]({author_url}) criou um quiz [{title}](<{url}f/p/{threadId}>) em {forumName} __[Ver imagem]({image_url})__ desconhecido {} etiquetas 